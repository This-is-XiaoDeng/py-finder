import os
import sys
import time
import threading

recursionlimit = 1000
leng = 1

path = []
def find(name,start,wait = 0.1):
    """查找文件\n  find(name=文件名,start=起始目录) => list\n建议把递归限制调大点不然很容易溢出"""
    global path,recursionlimit,leng

    files = os.listdir(start)
    
    print("正在从目录",start,"下的",files.__len__(),"个文件/目录(",files,")中查找",name)
    for file in files:
        if os.path.isdir(start + os.sep + file):
            # 是目录，递归
            leng += 1
            if leng >= recursionlimit:
                recursionlimit += 5
                sys.setrecursionlimit(recursionlimit)
            path += find(name,start + os.sep + file)
        else:
            # 是文件，查找
            if file.find(name) != -1:
                path += [start + os.sep + file]
                print("已记录文件",file)
        time.sleep(wait)    # 防卡死，如果将wait设置为0关掉这一功能
    print("目录",start,"索引完成")
    return path

def find_threads(name,start,wait = 0.05):
    global path,recursionlimit,leng

    files = os.listdir(start)
    
    print("正在从目录",start,"下的",files.__len__(),"个文件/目录(",files,")中查找",name)
    for file in files:
        if os.path.isdir(start + os.sep + file):
            # 是目录，递归
            thread = threading.Thread(target = find_threads,args = (name,start + os.sep + file,wait))
            thread.start()
        else:
            # 是文件，查找
            if file.find(name) != -1:
                path += [start + os.sep + file]
                print("已记录文件",file)
        time.sleep(wait)    # 防卡死，如果将wait设置为0关掉这一功能
    print("目录",start,"索引完成，正在等待thread")
    try:
        thread.join()
    except:
        pass
    return path
