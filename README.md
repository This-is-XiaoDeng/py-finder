# PyFinder

### 介绍
PyFinder是由 这里是小邓 编写的一个 Python 本地文件查找解决方案。此库支持传统递归查找和多线程查找，是Python文件查找较好的解决方案之一。

### 软件架构
主要文件结构：

py-finder

 |-README.md 自述文件

 |- pyfinder.py 主库文件

### 安装方法
1. 克隆本仓库
```bash
git clone https://gitee.com/This-is-XiaoDeng/py-finder.git
```
2. 将`pyfinder.py`复制到（您要使用这个库的程序）的目录下
3. 导入库
```python
import pyfinder
```
or
```python
from pyfinder import *
```

### 使用说明
> 我之后会上传几个Demo，到时候问题就不大了

#### 函数列表
| 函数 | 参数列表 | 支持最大文件夹深度 | 说明 | 返回值 |
|-----|-----|-----|-----|-----|
| find() | name = 文件名关键字  start=起始目录 wait=防死机等级(默认0.1) | 无限 | 传统单线程递归查找法（不受递归次数约束）| 符合筛选条件的文件路径列表(list) |
| find_threads() | name = 文件名关键字  start=起始目录 wait=防死机等级(默认0.01) | 约800层 | 多线程查找法（受`threading`最大线程影响）| 符合筛选条件的文件路径列表(list) |
#### 调用示例
```python
import pyfinder
f = pyfinder.find_threads(name="exe",start="C:\\")
print(f)
```
> 千万表像上面那么做，相信我，查找的时候永远也不要让符合筛选的文件过多，会内存溢出的（我就这么做过）

### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


### 许可证
此程序采用 MIT 许可证

### TODO
- [x] 完成多线程查找
- [ ] 写demo
- [ ] 打包上pypi
- [ ] 大力宣传
- [ ] 修复内存溢出的bug

### 贡献名单
> 代码
>> 这里是小邓

> 调试
>> 这里是小邓
>> 熊熊糖果